<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="{{ url('/themes/adm/assets/img/laravel.png') }}" />
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>InoveWS | @yield('title', 'Login')</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link href="{{ url('/themes/adm/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Font Awesome -->
	<link href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Ionicons -->
	<link href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Theme style -->
	<link href="{{ url('/themes/adm/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
	<!-- iCheck -->
	<link href="{{ url('/themes/adm/assets/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />

	<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
			]); ?>
	</script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


</head>
<body class="hold-transition login-page">
	<div class="container">
		@yield('content')
	</div>{{-- container --}}
	<script src="{{ url('/themes/adm/assets/plugins/jQuery/jQuery-2.1.3.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
	<script>
		$(document).ready(function () {
			{{-- Esconde avisos --}}
			$('.alert').delay(5000).fadeOut(1000);
		});
	</script>
</body>
</html>

