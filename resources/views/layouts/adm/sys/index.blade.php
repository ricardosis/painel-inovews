<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/x-icon" href="{{ url('/themes/adm/assets/img/laravel.png') }}" />
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>InoveWS | @yield('title')</title>
	<!-- bootstrap 3.0.2 -->
	<link href="{{ url('/themes/adm/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- font Awesome -->
	<link href="{{ url('/themes/adm/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Ionicons -->
	<link href="{{ url('/themes/adm/assets/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Morris chart -->
	<link href="{{ url('/themes/adm/assets/css/morris/morris.css') }}" rel="stylesheet" type="text/css" />
	<!-- jvectormap -->
	<link href="{{ url('/themes/adm/assets/css/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
	<!-- Date Picker -->
	<link href="{{ url('/themes/adm/assets/css/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
	<!-- Data Tables -->
	<link rel="stylesheet" href="{{ url('/themes/adm/assets/js/plugins/datatables/dataTables.bootstrap.css') }}" type="text/css"/>
	<!-- fullCalendar -->
	<!-- <link href="{{ url('/themes/adm/assets/css/fullcalendar/fullcalendar.css') }}" rel="stylesheet" type="text/css" /> -->
	<!-- Daterange picker -->
	<link href="{{ url('/themes/adm/assets/css/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
	<!-- iCheck for checkboxes and radio inputs -->
	<link href="{{ url('/themes/adm/assets/css/iCheck/all.css') }}" rel="stylesheet" type="text/css" />
	<!-- bootstrap wysihtml5 - text editor -->
	<!-- <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<!-- Theme style -->
	<link href="{{ url('/themes/adm/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    endif]-->
</head>
<body class="skin-black">
	@include('includes.header')
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<aside class="left-side sidebar-offcanvas">
			@include('includes.sidebar')
		</aside>
		<aside class="right-side">
			<section class="content">
				@yield('content-painel')
			</section>
			<div class="footer-main">
				&copy 2016 Inove WS - Todos os direitos reservados
			</div>
		</aside>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="{{ url('/themes/adm/assets/js/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/jquery-ui-1.10.3.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/chart.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/fullcalendar/fullcalendar.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/Director/app.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/Director/dashboard.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/datepicker/bootstrap-datepicker.js') }}"  type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript" ></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/custom.js') }}" type="text/javascript"></script>
	<script src="{{ url('/themes/adm/assets/js/plugins/holder.js') }}" type="text/javascript"></script>
</body>
</html>