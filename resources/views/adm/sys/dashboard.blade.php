{{-- CHAMA O LAYOUT DO PAINEL --}}
@extends('layouts.adm.sys.index')

{{-- AQUI VEM O NOME DA PÁGINA. --}}
@section('title', 'Dashboard' )

{{-- CHAMA O CONTEUDO DO PAINEL --}}
@section('content-painel')

{{-- <div class="row">
<div class="col-md-12">
	<ul class="breadcrumb">
		<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="#">Dashboard</a></li>
		<li class="active">Current page</li>
	</ul>
</div>
</div> --}}

<div class="row" style="margin-bottom:5px;">
	{{-- <div class="col-md-3">
	<div class="sm-st clearfix">
		<span class="sm-st-icon st-red"><i class="fa fa-check-square-o"></i></span>
		<div class="sm-st-info">
			<span>0</span>
			Total Tasks
		</div>
	</div>
</div> --}}
<div class="col-md-3">
	<div class="sm-st clearfix">
		<span class="sm-st-icon st-green"><i class="fa fa-line-chart "></i></span>
		<div class="sm-st-info">
			<span>0</span>
			Visitas Hoje
		</div>
	</div>
</div>
{{-- <div class="col-md-3">
<div class="sm-st clearfix">
	<span class="sm-st-icon st-blue"><i class="fa fa-dollar"></i></span>
	<div class="sm-st-info">
		<span>0</span>
		Total Profit
	</div>
</div>
</div>
<div class="col-md-3">
	<div class="sm-st clearfix">
		<span class="sm-st-icon st-violet"><i class="fa fa-envelope-o"></i></span>
		<div class="sm-st-info">
			<span>0</span>
			Total Documents
		</div>
	</div>
</div> --}}
</div>
<div class="row">
	<div class="col-md-4">
		<div class="panel">
			<div class="panel-body">
				<div class="text-center">
					<img src="/uploads/avatars/{{ Auth::user()->avatar }}" width="182px" class="img-circle" alt="User Image" />
				</div>
				<h4 class="text-center">{{ Auth::user()->name }}<span class="text-muted small"> - Administrador</span></h4>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel">
			<div class="panel-body">
				<input type="text" placeholder="Pesquisar..." class="form-control blog-search">
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<section class="panel">
			<header class="panel-heading">
				Atualizações
			</header>
			<div class="panel-body">
				Nenhum Atualização Disponível
			</div>
		</section>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
			<header class="panel-heading">
				Acesso Rápido
			</header>
			<div class="panel-body widget-quick-status-post">
			</div>
		</div>
	</div>
</div>
{{-- <div class="row">
<div class="col-md-7">
	<section class="panel tasks-widget">
		<header class="panel-heading">
			Últimos posts
		</header>
		<div class="panel-body">

			<div class="task-content">

				<ul class="task-list">
					<li>
						<div class="task-checkbox">

							<input type="checkbox" class="flat-grey list-child"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Director is Modern Dashboard</span>
							<span class="label label-success">2 Days</span>
							<div class="pull-right hidden-phone">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>
					<li>
						<div class="task-checkbox">
							<input type="checkbox" class="flat-grey"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Fully Responsive & Bootstrap 3.0.2 Compatible</span>
							<span class="label label-danger">Done</span>
							<div class="pull-right hidden-phone">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>
					<li>
						<div class="task-checkbox">
							<input type="checkbox" class="flat-grey"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Latest Design Concept</span>
							<span class="label label-warning">Company</span>
							<div class="pull-right hidden-phone">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>
					<li>
						<div class="task-checkbox">
							<input type="checkbox" class="flat-grey"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Write well documentation for this theme</span>
							<span class="label label-primary">3 Days</span>
							<div class="pull-right hidden-phone">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>
					<li>
						<div class="task-checkbox">
							<input type="checkbox" class="flat-grey"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Don't bother to download this Dashbord</span>
							<span class="label label-inverse">Now</span>
							<div class="pull-right">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>
					<li>
						<div class="task-checkbox">

							<input type="checkbox" class="flat-grey"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Give feedback for the template</span>
							<span class="label label-success">2 Days</span>
							<div class="pull-right hidden-phone">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>
					<li>
						<div class="task-checkbox">

							<input type="checkbox" class="flat-grey"/>
						</div>
						<div class="task-title">
							<span class="task-title-sp">Tell your friends about this admin template</span>
							<span class="label label-danger">Now</span>
							<div class="pull-right hidden-phone">
								<button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</li>

				</ul>
			</div>

		</div>
	</section>
</div>
</div> --}}

<div class="row" style="margin-bottom:5px;">
	<div class="col-sm-12">
		<section class="panel">
			<header class="panel-heading">
				Listar Usuários
			</header>
			<div class="box box-primary">
				<div class="box-body">
					<table id="tableView" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Usuário</th>
								<th>Data</th>
								<th>Status</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Paulo Ricardo</td>
								<td>26/09/2016 13:02</td>
								<td><span class="label label-inverse">Entrou</span></td>
								<td>O <b>usuário</b> entrou na administração do Painel.</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
</div>


@stop
