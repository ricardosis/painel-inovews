{{-- CHAMA O LAYOUT DO PAINEL --}}
@extends('layouts.adm.sys.index')

{{-- AQUI VEM O NOME DA PÁGINA. --}}
@section('title', 'Listar Configurações' )

{{-- CHAMA O CONTEUDO DO PAINEL --}}
@section('content-painel')

<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="{{ URL::route('Painel.Sys.index') }}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{ URL::route('Painel.Sys.Configuracoes.index') }}">Configurações</a></li>
            <li class="active">Listar Configurações</li>
		</ul>
	</div>
</div>

<div class="row" style="margin-bottom:5px;">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Listar Configurações
            </header>
            <div class="box box-primary">
                <div class="box-body"></div>
            </div>
        </section>
    </div>
</div>

@stop
