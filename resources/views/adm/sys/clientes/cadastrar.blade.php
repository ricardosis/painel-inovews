{{-- CHAMA O LAYOUT DO PAINEL --}}
@extends('layouts.adm.sys.index')

{{-- AQUI VEM O NOME DA PÁGINA. --}}
@section('title', 'Cadastrar Clientes' )

{{-- CHAMA O CONTEUDO DO PAINEL --}}
@section('content-painel')

<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="{{ URL::route('Painel.Sys.index') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ URL::route('Painel.Sys.Clientes.index') }}">Clientes</a></li>
			<li class="active">Cadastrar Cliente</li>
		</ul>
	</div>
</div>

<div class="row" style="margin-bottom:5px;">
	<div class="col-md-12">
		<form role="form" method="POST" action="{{ URL::route('Painel.Sys.Clientes.cadastrar.index') }}">
			{{ csrf_field() }}
			<div class="box box-primary">
				<div class="box-header"><h3 class="box-title">Dados do Cliente</h3></div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="nome">Nome</label>
										<input type="text" name="nome" class="form-control" id="nome" value="" placeholder="Informe o nome do Cliente">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<label for="cpf">CPF</label>
												<input type="text" name="cpf" class="form-control" id="cpf" value="" placeholder="Informe o número do CPF">
											</div>
											<div class="col-md-6">
												<label for="rg">RG</label>
												<input type="text" name="rg" class="form-control" id="rg" value="" placeholder="Informe o número do RG">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="data_nasc">Data de Nascimento</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="data_nasc" class="form-control datepicker" id="data_nasc" value="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="prof_ativ">Profissão ou Atividade</label>
										<input type="text" name="prof_ativ" class="form-control" id="prof_ativ" value="" placeholder="Informe a profissão ou atividade do Cliente">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<label for="estado">Estado</label>
												<select class="form-control m-bot15"name="estado" id="estado">
													<option value=""  selected="selected">Selecione...</option>
												</select>
											</div>
											<div class="col-md-6">
												<label for="cidade">Cidade</label>
												<select class="form-control m-bot15"name="cidade" id="cidade">
													<option value=""  selected="selected">Selecione...</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="endereco">Endereço</label>
										<input type="text" name="endereco" class="form-control" id="endereco" value="" placeholder="Informe o endereço do Cliente">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-3">
												<label for="numero">Número</label>
												<input type="text" name="numero" class="form-control" id="numero" value="" placeholder="Informe o número da casa do Cliente">
											</div>
											<div class="col-md-6">
												<label for="bairro">Bairro</label>
												<input type="text" name="numero" class="form-control" id="numero" value="" placeholder="Informe o bairro do casa do Cliente">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-4">
												<label for="tel_residencial">Telefone Residencial</label>
												<input type="text" name="tel_residencial" class="form-control" id="tel_residencial" value="" placeholder="Informe o número de telefone residencial do Cliente">
											</div>
											<div class="col-md-4">
												<label for="tel_comercial">Telefone Comercial</label>
												<input type="text" name="tel_comercial" class="form-control" id="tel_comercial" value="" placeholder="Informe o número de telefone comercial do Cliente">
											</div>
											<div class="col-md-4">
												<label for="tel_celular">Telefone Celular</label>
												<input type="text" name="tel_celular" class="form-control" id="tel_celular" value="" placeholder="Informe o número de telefone celular do Cliente">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="email">E-mail</label>
										<input type="text" name="email" class="form-control" id="email" value="" placeholder="Informe o endereço de e-mail do Cliente">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="data_registro">Data de Registro</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="data_registro" class="form-control datepicker" id="data_nasc" value="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="flagstatus">Ativo</label>
										<div class="col-md-12">
											<div class="radio">
												<label>
													<input type="radio" name="flagstatus" id="1" value="1" checked="">
													Sim
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="flagstatus" id="2" value="2">
													Não
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="foto">Foto do Cliente</label>
										<div class="help-image">
											<img data-src='holder.js/280x280/text:SEM FOTO' width='320'>
										</div>
										<input type="file" name="foto" class="form-control" id="foto">
										<p class="help-block">Selecione a foto do perfil do usuário.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<div class="box-footer">
											<input type="submit" name="cadastar" value="Cadastrar" class="btn btn-flat btn-primary"/>
											<input type="reset" name="cancelar" value="Cancelar" class="btn btn-flat btn-default"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@stop
