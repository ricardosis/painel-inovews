{{-- CHAMA O LAYOUT DO PAINEL --}}
@extends('layouts.adm.sys.index')

{{-- AQUI VEM O NOME DA PÁGINA. --}}
@section('title', 'Cadastrar Usuários' )

{{-- CHAMA O CONTEUDO DO PAINEL --}}
@section('content-painel')

<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="{{ URL::route('Painel.Sys.index') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ URL::route('Painel.Sys.Usuarios.index') }}">Usuários</a></li>
			<li class="active">Cadastrar Usuários</li>
		</ul>
	</div>
</div>

<div class="row" style="margin-bottom:5px;">
    <div class="col-md-12">
        <form role="form" method="POST" action="{{ URL::route('Painel.Sys.Usuarios.cadastrar.index') }}">
            {{ csrf_field() }}
            <div class="box box-primary">
                <div class="box-header"><h3 class="box-title">Dados do Perfil: {{ $user->name }}</h3></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="nome">Nome</label>
                                        <input type="text" name="nome" class="form-control" id="nome" value="{{ $user->name }}" placeholder="Informe o nome do usuário">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="lastname">Sobrenome</label>
                                        <input type="text" name="lastname" class="form-control" id="lastname" value="{{ $user->lastname }}" placeholder="Informe o sobrenome do usuário">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="created_at">Data de Registro</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="created_at" class="form-control datepicker" id="created_at" value="{{ $user->created_at }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="tipo">Tipo</label>
                                        <select name="tipo" class="form-control" id="tipo">
                                         <option value=""  selected="selected">Selecione...</option>
                                         <option value="1" >Administrador</option>
                                         <option value="2" >Usuário</option>
                                     </select>
                                 </div>
                             </div>
                         </div>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-md-4">
                                     <label for="empresa">Empresa</label>
                                     <select name="empresa" class="form-control" id="empresa">
                                         <option value=""  selected="selected">Selecione...</option>
                                         <option value='1'>Inove WS</option>
                                     </select>
                                 </div>
                             </div>
                         </div>
                         <div class="form-group">
                             <div class="row">
                               <div class="col-md-6">
                                   <label for="flagstatus">Ativo</label>
                                   <div class="col-md-12">
                                      <div class="radio">
                                          <label>
                                              <input type="radio" name="flagstatus" id="1" value="1" checked="">
                                              Sim
                                          </label>
                                      </div>
                                      <div class="radio">
                                          <label>
                                              <input type="radio" name="flagstatus" id="2" value="2">
                                              Não
                                          </label>
                                      </div>
                                    </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="foto">Foto do Perfil</label>
                                <input type="file" name="foto" class="form-control" id="foto">
                                <p class="help-block">Selecione a foto do perfil do usuário.</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="acaoes">Ações</label>
                      <div class="col-sm-12">
                        <div class="minimal-blue single-row">
                          <div class="checkbox ">
                            <input type="checkbox" checked disabled>
                            <label>Cadastrar</label>
                          </div>
                        </div>
                        <div class="minimal-blue single-row">
                          <div class="checkbox ">
                            <input type="checkbox" disabled>
                            <label>Editar</label>
                          </div>
                        </div>
                        <div class="minimal-blue single-row">
                          <div class="checkbox ">
                            <input type="checkbox" disabled>
                            <label>Excluir</label>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="form-group">
                      <label for="acaoes">Categorias</label>
                      <div class="col-sm-12">
                        <div class="minimal-blue single-row">
                          <div class="checkbox ">
                            <input type="checkbox" checked disabled>
                            <label>Geral</label>
                          </div>
                        </div>
                        <div class="minimal-blue single-row">
                          <div class="checkbox ">
                            <input type="checkbox" disabled>
                            <label>Financeiro</label>
                          </div>
                        </div>
                        <div class="minimal-blue single-row">
                          <div class="checkbox ">
                            <input type="checkbox" disabled>
                            <label>Suporte</label>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header"><h3 class="box-title">Dados de Acesso</h3></div>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <label for="login">Login</label>
                        <input type="text" name="login" class="form-control" id="login" value="" placeholder="Informe o login do usuário">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" class="form-control" id="senha" value="" placeholder="Informe a senha do usuário">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" name="cadastar" value="Cadastrar" class="btn btn-flat btn-primary"/>
            <input type="reset" name="cancelar" value="Cancelar" class="btn btn-flat btn-default"/>
        </div>
    </div>
</form>
</div>
</div>

@stop
