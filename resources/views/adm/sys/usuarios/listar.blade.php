{{-- CHAMA O LAYOUT DO PAINEL --}}
@extends('layouts.adm.sys.index')

{{-- AQUI VEM O NOME DA PÁGINA. --}}
@section('title', 'Cadastrar Usuários' )

{{-- CHAMA O CONTEUDO DO PAINEL --}}
@section('content-painel')

<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="{{ URL::route('Painel.Sys.index') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ URL::route('Painel.Sys.Usuarios.index') }}">Usuários</a></li>
			<li class="active">Listar Usuários</li>
		</ul>
	</div>
</div>

<div class="row" style="margin-bottom:5px;">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Listar Usuários
            </header>
            <div class="box box-primary">
                <div class="box-body">
                    <table id="tableView" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Usuário</th>
                                <th>Login</th>
                                <th>Empresa</th>
                                <th>Tipo</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td><a href="#">Paulo Ricardo</a></td>
                                <td>ricardo@godoi.xyz</td>
                                <td>Inove WS</td>
                                <td>Administrador</td>
                                <td><span class='label label-success'>Ativo</span></td>
                                <td>
                                    <a href="#" class="btn btn-flat btn-danger btn-sm" title="Bloquear"><i class="fa fa-times"></i> Bloquear</a>
                                    <a href="#" class="btn btn-flat btn-primary btn-sm" title="Editar"><i class="fa fa-pencil"></i> Editar</a>
                                    <a href="#" class="btn btn-flat btn-danger btn-sm" title="Excluir"><i class="fa fa-trash"></i> Excluir</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

@stop
