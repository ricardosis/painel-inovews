<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>InoveWS | {{ $title }}</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="{{ url('/') }}/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/adm/assets/css/login.css">
	<link rel="stylesheet" href="{{ url('/') }}/adm/assets/plugins/iCheck/square/blue.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="sys/index"><b>Inove</b>WS</a>
		</div>
		<div class="login-box-body">
			<div class="login-logo"><img src="{{ url('/') }}/adm/assets/img/godoi.xyz.png" width='230'></div>
			<p class="login-box-msg"></p>
			<form action="{{ url('/') }}/adm/app/login" method="post">
				<div class="form-group has-feedback">
					<input type="email" class="form-control" placeholder="E-mail">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Senha">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary btn-block btn-sample">Entrar</button>
					</div>
				</div>
			</form>
		</div>
		<div class="login-rolimnet">
			Copyright © godoi.xyz <br/> <a href="{{ url('/') }}/politicadeprivacidade">Política de Privacidade</a> | <a href="{{ url('/') }}/termosdeuso">Termos de uso</a>
		</div>
		<div class="login-copyright">
			Sistema licenciado por:
			<img src="assets/img/godoi.xyz.png" class="login-logo-inovews">
			<p>beta 0.1a - adminex</br>
				InoveWS admin</p>
			</div>
		</div>
	</div>

	<script src="{{ url('/') }}/adm/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="{{ url('/') }}/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/adm/assets/plugins/iCheck/icheck.min.js"></script>
	<script>
		$(document).ready(function () {
			/*Esconde avisos*/
			$('.alert').delay(5000).fadeOut(1000);
		});
	</script>
</body>
</html>