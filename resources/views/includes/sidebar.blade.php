<section class="sidebar">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left">
					<img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="img-circle" width="20px" height="20px" alt="User Image" />
				</div>
				<div class="pull-left info">
					<p>Olá, {{ Auth::user()->name }}</p>
					{{--<p class"text">Administrador</p>--}}
					<a href="{{ URL::route('Painel.Sys.index') }}"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>
			<!-- search form -->
			<form action="#" method="get" class="sidebar-form">
				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="Pesquisar..."/>
					<span class="input-group-btn">
						<button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>
			<!-- /.search form -->
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu">
				{{-- <li class="active"> --}}
				<li class="{{ Request::segment(3) === 'dashboard' ? 'active' : null | Request::segment(3) === 'home' ? 'active' : null || Request::segment(3) === null ? 'active' : null}}">
					<a href="{{ URL::route('Painel.Sys.dashboard.index') }}">
						<i class="fa fa-dashboard"></i><span>Dashboard</span>
					</a>
				</li>
				<li class="treeview {{ Request::segment(3) === 'clientes' ? 'active' : null }}">
					<a href="#">
						<i class="fa fa-users"></i>
						<span>Clientes</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li class="{{ Request::segment(4) === 'cadastrar' ? 'active' : null }}">
							<a href="{{ URL::route('Painel.Sys.Clientes.cadastrar.index') }}"><i class="fa fa-angle-double-right"></i> Novo Cadastro</a>
						</li>
						<li>
							<a href="{{ URL::route('Painel.Sys.Clientes.index') }}"><i class="fa fa-angle-double-right"></i> Listar Cadastros</a>
						</li>
					</ul>
				</li>
				<li class="treeview {{ Request::segment(3) === 'configuracoes' ? 'active' : null }}">
					<a href="#">
						<i class="fa fa-cogs"></i>
						<span>Configurações</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li class="{{ Request::segment(4) === 'configuracoes' ? 'active' : null }}">
							<a href="{{ URL::route('Painel.Sys.Configuracoes.cadastrar.index') }}"><i class="fa fa-angle-double-right"></i> Novo Cadastro</a>
						</li>
						<li>
							<a href="{{ URL::route('Painel.Sys.Configuracoes.index') }}"><i class="fa fa-angle-double-right"></i> Listar Cadastros</a>
						</li>
					</ul>
				</li>
				<li class="treeview {{ Request::segment(3) === 'usuarios' ? 'active' : null }}">
					<a href="#">
						<i class="fa fa-users"></i>
						<span>Usuários</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li class="{{ Request::segment(4) === 'cadastrar' ? 'active' : null }}">
							<a href="{{ URL::route('Painel.Sys.Usuarios.cadastrar.index') }}"><i class="fa fa-angle-double-right"></i> Novo Cadastro</a>
						</li>
						<li>
							<a href="{{ URL::route('Painel.Sys.Usuarios.index') }}"><i class="fa fa-angle-double-right"></i> Listar Cadastros</a>
						</li>
					</ul>
				</li>

				<li>
					<a href="{{ url('/adm/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						<i class="fa fa-sign-in"></i> <span>Sair</span>
					</a>
				</li>

			</ul>
		</section>