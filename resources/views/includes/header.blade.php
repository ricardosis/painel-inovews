<header class="header">
		<a href="{{ URL::route('Painel.Sys.home.index') }}" class="logo">
			{{--<img src="{{ url('/themes/adm/assets/img/godoi.xyz.png') }}" >--}}
			Inove <b>WS</b>
		</a>
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li class="dropdown messages-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-envelope"></i>
							<span class="label label-success">4</span>
						</a>
						{{-- <ul class="dropdown-menu"> <!-- INICIO DO DROPBOX  -->
						<li class="header">You have 4 messages</li>
						<li>
							<ul class="menu">
								<li>
									<a href="#">
										<div class="pull-left">
											<img src="img/26115.jpg" class="img-circle" alt="User Image"/>
										</div>
										<h4>
											Support Team
										</h4>
										<p>Why not buy a new awesome theme?</p>
										<small class="pull-right"><i class="fa fa-clock-o"></i> 5 mins</small>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="pull-left">
											<img src="img/26115.jpg" class="img-circle" alt="user image"/>
										</div>
										<h4>
											Director Design Team

										</h4>
										<p>Why not buy a new awesome theme?</p>
										<small class="pull-right"><i class="fa fa-clock-o"></i> 2 hours</small>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="pull-left">
											<img src="img/avatar.png" class="img-circle" alt="user image"/>
										</div>
										<h4>
											Developers

										</h4>
										<p>Why not buy a new awesome theme?</p>
										<small class="pull-right"><i class="fa fa-clock-o"></i> Today</small>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="pull-left">
											<img src="img/26115.jpg" class="img-circle" alt="user image"/>
										</div>
										<h4>
											Sales Department

										</h4>
										<p>Why not buy a new awesome theme?</p>
										<small class="pull-right"><i class="fa fa-clock-o"></i> Yesterday</small>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="pull-left">
											<img src="img/avatar.png" class="img-circle" alt="user image"/>
										</div>
										<h4>
											Reviewers

										</h4>
										<p>Why not buy a new awesome theme?</p>
										<small class="pull-right"><i class="fa fa-clock-o"></i> 2 days</small>
									</a>
								</li>
							</ul>
						</li>
						<li class="footer"><a href="#">See All Messages</a></li>
					</ul> --}}
				</li>
				<li class="dropdown tasks-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-tasks"></i>
						<span class="label label-danger">9</span>
					</a>
					{{-- <ul class="dropdown-menu">
					<li class="header">You have 9 tasks</li>
					<li>
						<!-- inner menu: contains the actual data -->
						<ul class="menu">
							<li><!-- Task item -->
								<a href="#">
									<h3>
										Design some buttons
										<small class="pull-right">20%</small>
									</h3>
									<div class="progress progress-striped xs">
										<div class="progress-bar progress-bar-success" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">20% Complete</span>
										</div>
									</div>
								</a>
							</li><!-- end task item -->
							<li><!-- Task item -->
								<a href="#">
									<h3>
										Create a nice theme
										<small class="pull-right">40%</small>
									</h3>
									<div class="progress progress-striped xs">
										<div class="progress-bar progress-bar-danger" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">40% Complete</span>
										</div>
									</div>
								</a>
							</li><!-- end task item -->
							<li><!-- Task item -->
								<a href="#">
									<h3>
										Some task I need to do
										<small class="pull-right">60%</small>
									</h3>
									<div class="progress progress-striped xs">
										<div class="progress-bar progress-bar-info" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">60% Complete</span>
										</div>
									</div>
								</a>
							</li><!-- end task item -->
							<li><!-- Task item -->
								<a href="#">
									<h3>
										Make beautiful transitions
										<small class="pull-right">80%</small>
									</h3>
									<div class="progress progress-striped xs">
										<div class="progress-bar progress-bar-warning" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">80% Complete</span>
										</div>
									</div>
								</a>
							</li><!-- end task item -->
						</ul>
					</li>
					<li class="footer">
						<a href="#">View all tasks</a>
					</li>
				</ul> --}}
			</li>
			<li class="dropdown user user-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="img-circle" width="16px" height="16px" alt="User Image" /><span>{{ Auth::user()->name }}<i class="caret"></i></span></a>
				<ul class="dropdown-menu dropdown-custom dropdown-menu-right">
					<li class="dropdown-header text-center">Conta</li>
					<li>
						<a href="#">
							<i class="fa fa-clock-o fa-fw pull-right"></i>
							<span class="badge badge-success pull-right">10</span> Atualizações
						</a>
						<a href="#">
							<i class="fa fa-envelope-o fa-fw pull-right"></i>
							<span class="badge badge-danger pull-right">5</span> Mensagens
						</a>
						<a href="#">
							<i class="fa fa-magnet fa-fw pull-right"></i>
							<span class="badge badge-info pull-right">3</span> Assinaturas
						</a>
						<a href="#">
							<i class="fa fa-question fa-fw pull-right"></i>
							<span class="badge pull-right">11</span> FAQ
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="{{ URL::route('Painel.Sys.Usuarios.profile.index') }}">
							<i class="fa fa-user fa-fw pull-right"></i>Perfil
						</a>
						<a data-toggle="modal" href="#modal-user-settings">
							<i class="fa fa-cog fa-fw pull-right"></i>Configurações
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="{{ url('/adm/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-in fa-fw pull-right"></i> Sair</a>
					</li>
					<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</ul>
			</li>
		</ul>
	</div>
</nav>
</header>