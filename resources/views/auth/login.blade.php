@extends('layouts.adm.login')

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ URL::route('Painel.Sys.index') }}"><b>Inove</b>WS</a>
    </div>
    <div class="login-box-body">
        <div class="login-logo">
            <img src="{{ url('/themes/adm/assets/img/godoi.xyz.png') }}" width='230'>
        </div>
        <p class="login-box-msg"></p>
        <form class="form" role="form" method="POST" action="{{ url('adm/login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" name="password" placeholder="Senha" required>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block ">Entrar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="login-rolimnet">
        Copyright © godoi.xyz <br/> <a href="{{ url('/') }}/politicadeprivacidade">Política de Privacidade</a> | <a href="{{ url('/') }}/termosdeuso">Termos de uso</a>
    </div>
    <div class="login-copyright">
        Sistema licenciado por:
        <img src="{{ url('/themes/adm/assets/img/godoi.xyz.png') }}" class="login-logo-inovews">
        <p>beta 0.1a - director</br>InoveWS admin</p>
    </div>
</div>
@endsection
