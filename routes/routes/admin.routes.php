<?php

Route::group([ 'as' => 'Painel.', 'prefix' => 'adm'], function () {
	Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');

	//URI do Painel Administrativo/Sys
	Route::group([ 'as' => 'Sys.', 'prefix' => 'sys'], function () {
		/*Route::get('/', function () {
			//
		});*/
		Route::resource('/', 'Sys\IndexPainelController@index');
		Route::resource('home', 'Sys\IndexPainelController@index');
		Route::resource('dashboard', 'Sys\IndexPainelController@index');

		Route::group(['as' => 'Clientes.','prefix' => 'clientes'], function () {
			Route::resource('/', 'Sys\ClientesController@index');
			Route::resource('cadastrar', 'Sys\ClientesController@cadastrar');
		});

		Route::group(['as' => 'Configuracoes.','prefix' => 'configuracoes'], function () {
			Route::resource('/', 'Sys\ConfiguracoesController@index');
			Route::resource('cadastrar', 'Sys\ConfiguracoesController@cadastrar');
		});	

		Route::group(['as' => 'Usuarios.','prefix' => 'usuarios'], function () {
			Route::resource('/', 'Sys\UsuariosController@index');
			Route::resource('cadastrar', 'Sys\UsuariosController@cadastrar');
			Route::resource('profile', 'Sys\UsuariosController@profile');
			Route::resource('{id}/editar', 'Sys\UsuariosController@edit');
			Route::post('{id}/update', 'Sys\UsuariosController@update');
		});	
	});
});
/*
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout');
*/